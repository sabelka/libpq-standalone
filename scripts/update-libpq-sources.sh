#! /bin/sh
set -e

task="update"

function xcp()
{
    if [ $task = "update" ] ; then
        d=`dirname $2`
        if ! [ -d "$d" ] ; then
            echo "Creating directory $2"
            mkdir -p "$d"
        fi
        if ! [ -f $2 ] || ! cmp $1 $2 ; then
            echo "Updating $2"
            cp $1 $2
        fi
    elif [ $task = "clear" ] ; then
        if [ -f "$2" ] ; then
            echo "Removing $2"
            rm -f $2
            find "$d" -maxdepth 0 -empty -exec echo "Removing directory" {} \; -exec rmdir {} \;
        fi
    fi
}


if [ "$1" = "clear" ] ; then
    task="clear"
fi

pg_src=${HOME}/src/postgresql
dst_dir=$(dirname $0)/../libpq
dst_dir=$(realpath $dst_dir)

echo "Copying from $pg_src -> $dst_dir"

libpq_src="
    common/base64.c
    common/cryptohash.c
    common/encnames.c
    common/hmac.c
    common/ip.c
    common/link-canary.c
    common/md5.c
    common/md5_common.c
    common/md5_int.h
    common/pg_prng.c
    common/saslprep.c
    common/scram-common.c
    common/sha1.c
    common/sha1_int.h
    common/sha2.c
    common/sha2_int.h
    common/string.c
    common/unicode_norm.c
    common/wchar.c
    include/pg_config_manual.h
    include/postgres_ext.h
    include/postgres_fe.h
    include/port.h
    include/c.h
    include/common/base64.h
    include/common/config_info.h
    include/common/cryptohash.h
    include/common/fe_memutils.h
    include/common/hmac.h
    include/common/ip.h
    include/common/link-canary.h
    include/common/md5.h
    include/common/openssl.h
    include/common/pg_prng.h
    include/common/saslprep.h
    include/common/scram-common.h
    include/common/sha1.h
    include/common/sha2.h
    include/common/string.h
    include/common/unicode_nonspacing_table.h
    include/common/unicode_norm.h
    include/common/unicode_norm_table.h
    include/common/unicode_east_asian_fw_table.h
    include/lib/stringinfo.h
    include/libpq/pqcomm.h
    include/libpq/libpq-fs.h
    include/mb/pg_wchar.h
    include/port/pg_bitutils.h
    include/port/pg_bswap.h
    include/port/pg_crc32c.h
    include/port/simd.h
    include/port/win32_port.h
    interfaces/libpq/fe-auth-scram.c
    interfaces/libpq/fe-connect.c
    interfaces/libpq/fe-exec.c
    interfaces/libpq/fe-lobj.c
    interfaces/libpq/fe-misc.c
    interfaces/libpq/fe-print.c
    interfaces/libpq/fe-protocol3.c
    interfaces/libpq/fe-secure.c
    interfaces/libpq/fe-secure-common.c
    interfaces/libpq/fe-secure-common.h
    interfaces/libpq/fe-secure-openssl.c
    interfaces/libpq/fe-trace.c
    interfaces/libpq/legacy-pqsignal.c
    interfaces/libpq/libpq-events.c
    interfaces/libpq/libpq-events.h
    interfaces/libpq/libpq-fe.h
    interfaces/libpq/libpq-int.h
    interfaces/libpq/pqexpbuffer.c
    interfaces/libpq/pqexpbuffer.h
    interfaces/libpq/pthread-win32.c
    interfaces/libpq/fe-auth.c
    interfaces/libpq/fe-auth.h
    interfaces/libpq/fe-auth-sasl.h
    interfaces/libpq/win32.c
    interfaces/libpq/win32.h
    port/chklocale.c
    port/dirmod.c
    port/explicit_bzero.c
    port/getpeereid.c
    port/inet_aton.c
    port/inet_net_ntop.c
    port/noblock.c
    port/open.c
    port/pgsleep.c
    port/pgstrcasecmp.c
    port/pthread-win32.h
    port/pg_strong_random.c
    port/snprintf.c
    port/strerror.c
    port/strlcpy.c
    port/thread.c
    port/win32error.c
    port/win32setlocale.c
    include/port/win32/arpa/inet.h
    include/port/win32/netdb.h
    include/port/win32/netinet/in.h
    include/port/win32/pwd.h
    include/port/win32_msvc/sys/file.h
    include/port/win32_msvc/sys/param.h
    include/port/win32/sys/socket.h
    include/port/win32_msvc/sys/time.h
    include/port/win32_msvc/unistd.h
"
    
for f in $libpq_src ; do
    xcp $pg_src/src/$f $dst_dir/$f
done





# FIXME: adapt DEFAULT_PGSOCKET_DIR

# FIXME: genrate instead of copy
#xcp $pg_src/build/src/include/pg_config_ext.h $dst_dir/include/pg_config_ext.h
#xcp $pg_src/build/src/include/pg_config_os.h $dst_dir/include/pg_config_os.h
#xcp $pg_src/build/src/port/pg_config_paths.h $dst_dir/include/pg_config_paths.h
